# *opendata.cern.ch* on CERN OpenShift

For all interactions with OpenShift you first need to login:

```console
# If you don't have `oc` client you can use the docker image
# docker run -it --rm gitlab-registry.cern.ch/paas-tools/openshift-client /bin/bash
$ oc login https://openshift.cern.ch
Authentication required for https://openshift.cern.ch:443 (CERN)
Username: username
Password:
Login successful.
```

## COD Deployment

Clone this repo and replace the example secret on `manifests/buildsconfigs/web.yaml` and create an OpenShift secret with its value:

```console
# Create a secret (do not include `/` inside the secret string)
$ openssl rand -base64 32
It4NaJVEr9APcRWrYKrNtc1F681HpGKWqou3hOYpu8s=
$ sed -i 's/supersecretsecret/It4NaJVEr9APcRWrYKrNtc1F681HpGKWqou3hOYpu8s=/' \
  manifests/buildconfigs/web.yaml
$ oc create secret generic \
  --from-literal='WEBHOOK_SECRET=It4NaJVEr9APcRWrYKrNtc1F681HpGKWqou3hOYpu8s=' \
  generic-build-webhook
secret "generic-build-webhook" created
```

We will need as well to create the secret which holds the database password:
```console
$ POSTGRESQL_PASSWORD=$(openssl rand -hex 8)
$ oc create secret generic \
  --from-literal="POSTGRESQL_PASSWORD=$POSTGRESQL_PASSWORD" \
  --from-literal="SQLALCHEMY_DB_URI=postgresql+psycopg2://cernopendata:$POSTGRESQL_PASSWORD@db:5432/cernopendata" \
  cernopendata-db-password
secret "cernopendata-db-password" created
```

Another one for Elasticsearch password:
```console
$ ELASTICSEARCH_PASSWORD=$(openssl rand -hex 8)
$ oc create secret generic \
  --from-literal="ELASTICSEARCH_PASSWORD=$ELASTICSEARCH_PASSWORD" \
  elasticsearch-password
secret "elasticsearch-password" created
```

Also we will create the secret which holds the rabbitMQ password:
```console
$ RABBITMQ_DEFAULT_PASS=$(openssl rand -hex 8)
$ oc create secret generic \
  --from-literal="RABBITMQ_DEFAULT_PASS=$RABBITMQ_DEFAULT_PASS" \
  --from-literal="APP_CELERY_BROKER_URL=amqp://guest:$RABBITMQ_DEFAULT_PASS@mq:5672/" \
  mq-password
secret "mq-password" created
```

Create all OpenShift objects using the manifests as follows:

```console
$ oc create -Rf manifests/
# Build necessary images
$ oc start-build search --follow
$ oc start-build web --follow
```

Wait for all components to be ready (`current` column looks like the following):

```console
$ oc get deploymentconfig --watch
NAME         REVISION   DESIRED   CURRENT   TRIGGERED BY
cache    1          1         1         config
db       1          1         1         config
mq       1          1         1         config
proxy    1          1         1         config,image(nginx:latest)
search   1          1         1         config,image(elasticsearch:latest)
web      1          1         1         config,image(web:latest)
worker   1          1         1         config,image(web:latest)
```

Once ready, ssh to the web node and populate the instance (if not pre lifecycle hook is used):

```console
$ oc rsh dc/web
> ./code/scripts/populate-instance.sh
```

Finally, you can access CERN Open Data at http://opendata-dev.web.cern.ch

## How the `web` instance gets updated

![Build process diagram](images/diagram.jpeg)

### Images

`ImageStreams` are OpenShift managed docker images.

Since COD is using `centos:7` we create a `ImageStream` object poiting to this upstream image and another `ImageStream` for the resulting image:

```yaml
---
kind: "ImageStream"
apiVersion: "v1"
metadata:
  name: "centos"
spec:
  tags:
  - name: "7"
    from:
      kind: DockerImage
      name: "docker.io/centos:7"
```

```yaml
---
kind: "ImageStream"
apiVersion: "v1"
metadata:
  name: "web"
spec:
  tags:
  - name: "latest"
```

### Build

Now we want to specify how Invenio needs to be built using the a `BuildConfig`:

```yaml
---
kind: BuildConfig
apiVersion: v1
metadata:
  name: web
spec:
  source:
    type: "Git"
    git:
      uri: "https://github.com/cernopendata/opendata.cern.ch"
      ref: "master"
  strategy:
    type: "Docker"
    dockerStrategy:
      from:
        kind: "ImageStreamTag"
        name: "centos:7"
  output:
    to:
      kind: "ImageStreamTag"
      name: "web:latest"
  resources:
    requests:
      cpu: 1
      memory: 256Mi
    limits:
      cpu: 2
      memory: 512Mi
  triggers:
    - type: "ImageChange"
      imageChange: {}
    - type: "GitHub"
      github:
        secret: "securestring"
    - type: "Generic"
      generic:
        secret: "supersecretsecret" # example secret REGENERATE when deploying
```

Lets got quickly through the first three elements of the spec:

- Source: Where is the code coming from. A git repo with the corresponding branch.
- Strategy: There is the possibility to provide only code and OpenShift creates the image by itself. However, in our case, since we have dockerfiles, we use `docker` type. With the from attribute we are overwriting the dockerfile's FROM to point to the `ImageStream` we previously created.
- Output: where to send the image. The output of the build could be pushed to a docker registry, but in our case we are storing it in the previously created `ImageStream` web.

Note: The resources attribute is needed for performance reasons, if left by default Invenio build is very slow.

### Triggering the build

As you might noticed, we have ignored the last part of the `BuildConfig`.

```yaml
  triggers:
    - type: "ImageChange"
      imageChange: {}
    - type: "GitHub"
      github:
        secret: "securestring"
    - type: "Generic"
      generic:
        secret: "supersecretsecret"
```

This part defines when we want to rebuild the image.

* We have the option to rebuild when upstream `docker.io/centos:7` sha256 changes.
* Using a weebhook with GitHub, i.e. every commit, every tag ...
* Using a custom webhook.

### Automatic deployment

When you want an automatic deployment, all you need to add to your `DeploymentConfig` is (code taken from the last lines of `manifests/deployments/web.yaml`):

```yaml
  triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
          - web
        from:
          kind: ImageStreamTag
          name: web:latest
```

So every time the `web` image changes a pod with the last image is deployed because of two reasons:

* Last updates on upstream base image `centos:7`
* Last CERN Open Data github code.

## How deployment cycle works right now

A custom webhook has been created to trigger a nightly build of CERN Open Data. This is achieved using a `CronJob` (file `manifests/cronjobs/nightly.yaml`):

```yaml
---
apiVersion: batch/v2alpha1
kind: CronJob
metadata:
  name: nightly-build-trigger
spec:
  schedule: "0 0 * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: nightly-build-trigger
            image: tutum/curl
            command: ["curl", "-X", "POST", "-k", "-H", "'Content-Type: application/json'", "https://openshift-dev.cern.ch:443/oapi/v1/namespaces/test-opendata-dev/buildconfigs/web/webhooks/$(WEBHOOK_SECRET)/generic"]
            env:
              - name: WEBHOOK_SECRET
                valueFrom:
                  secretKeyRef:
                    name: generic-build-webhook
                    key: WEBHOOK_SECRET
          restartPolicy: OnFailure
```

Which triggers the `manifests/buildconfigs/web.yaml`, which creates a new image and because we added an `ImageChange` trigger to `manifests/deployments/web.yaml` a new deployment of the application will be started.
